# Implement IBM Cloud Function Endpoints

## 1.You will need your Cloudant service credentials.

  1. Navigate to the resources page - https://cloud.ibm.com/resources.
   
  2. Click on the Cloudant service. If you don't have one already, create one here - https://cloud.ibm.com/catalog/services/cloudant     and choose Authentication method IAM

3.	Click on Service credentials on the left bar.

4.	Click New credential. You can leave the default options on the popup.

5.	Expand the newly created credentials and take note of the apikey and the url.

6.	Before you import the data, create one databases using the Cloudant UI:

7.	Click Launch Dashboard on the Cloudant UI and then Click Create Database

8.	Create one database called pets-database with Non-partitioned

# 2. Load data into the database

In Theia terminal, you can use the npm package couchimport to load data into your database. Note you may need to install couchimport first:

sudo npm install -g couchimport

Set the following environment variables using the credentials in the newly created Service credentials:

export IAM_API_KEY="REPLACED IT WITH GENERATED `apikey`"

export COUCH_URL="REPLACED IT WITH GENERATED `url`"

Use the following command to import the JSON data into the database.

Import petshop,json data into pets-database database

Navigate to the folder where petshop,json is available

Then run command given below

cat ./ petshop,json | couchimport --type "json" --jsonpath " intents.*" --database pets-database 

You should now have one database, pets-database with sample data in database

## 3. Create and Invoke an Action for Both GET AND POST and API End Points using Node.js

### Post method to insert Data in Cloudant Database pets-database 

1.	Go to the IBM Cloud Functions UI.
2.	Click Start Creating.
3.	Click Action in order to create an action.
4.	Name your action Post-Pets-Data or whatever you want
5.	Click Create Package in order to put this action into a package.
6.	Name the package API-Methods since we'll make several actions that perform string manipulation.
7.	Click Create.
8.	Choose a Node.js runtime 
9.	Click Create.
10.	Use the following code for the main function in the code editor.

          const Cloudant = require('@cloudant/cloudant');


          async function main(params) {
   
          secret={
         "COUCH_URL": "cloudant credential URL",
          "IAM_API_KEY": "cloudant credential api-key",
          "COUCH_USERNAME": "cloudant credential Username "
         };
          const cloudant = Cloudant({
         url: secret.COUCH_URL,
         plugins: { iamauth: { iamApiKey: secret.IAM_API_KEY } }
          });
 
 
        try {
        
         let dbList = await cloudant.db.use("pets-database");
         let res=""
         res= await dbList.insert(params)
         return {"submitted":true} ;
          } catch (error) {
         return { error: error.description };
         }
         }


11.	Click Save.

12.	Click Invoke with parameters
13.	Put the following in the input box:

         {

        "intent": "Cancel",

         "examples": [

         {

          "text": "cancel that"

         },

         {

          "text": "cancel the request"

         },

         {

          "text": "forget it"

         },

         {

          "text": "i changed my mind"

         },

         {

          "text": "i don't want a table anymore anymore"

         },

         {

          "text": "never mind"

         },

         {

          "text": "nevermind"

         }

        ],

        "description": "Cancel the current request"

        }

14. Click Apply.
15.	Click Invoke.

You will see this output

      {

        "submitted": true

      }

This means your data successfully inserted into the pets-database database you can see in your pets-database database one extra documents created

16. Now we will create API Ends Point for this function

click Endpoints in the navigation menu.

Click the checkbox for Enable as Web Action.

Return to the main IBM Cloud Functions page

Click APIs.

Click Create API.

For API name, put api. This will also create the base path.

Click Create operation.

Put /post-data as the path.

Use POST for the verb, choose Package containing action API-Methods and choose Post-Pets-Data for the action.

Click Create.

Scroll to the bottom and click Save.

The API route. This is the domain and the base path.

And this is your end point

https://c0757ccd.eu-gb.apigw.appdomain.cloud/api/post-data

You have successfully created API End Point for inserting data into pets-database database now you can check in postman or any an API client which is used to create, share, test and document APIs.

## Get method to get Data from Cloudant Database pets-database using Node.js

1.	Go to the IBM Cloud Functions UI.
2.	Click Start Creating.
3.	Click Action in order to create an action.
4.	Name your action Get-Pets-Data or whatever you want
5.	Select Package API-Methods in order to put this action into a package.
6.	This time do not have to create package again we selected existing package.
7.	Choose a Node.js runtime 
8.	Click Create.
9.	Use the following code for the main function in the code editor.

         const Cloudant = require('@cloudant/cloudant');

         async function main(params) {
         secret ={

         "COUCH_URL": "cloudant credential URL",
         "IAM_API_KEY": "cloudant credential api-key",
         "COUCH_USERNAME": "cloudant credential Username "
          };

        const cloudant = Cloudant({
         url: secret.COUCH_URL,
         plugins: { iamauth: { iamApiKey: secret.IAM_API_KEY } }
        });

        try {
         let dbList = await cloudant.db.use("pets-database");
         
         let res=""
        res=await dbList.list({include_docs: true}); 
         return { "dbs": res };
        } catch (error) {
         return { error: error.description };
         }
 
        } 
10.	Click Save.

11.	Click Invoke.
You will see this output 
         {

         "dbs": {

         "offset": 0,

         "rows": [

         {

        "doc": {

          "_id": "3c237ccee584e6009771b393e35cd599",

          "_rev": "1-e0707582e082e902ccf47dd68ab11afd",

          "description": "Cancel the current request",

          "examples": [

            {

              "text": "cancel that"

            },

            {

              "text": "cancel the request"

            },

            {

              "text": "forget it"

            },

            {

              "text": "i changed my mind"

            },

            {

              "text": "i don't want a table anymore anymore"

            },

            {

              "text": "never mind"

            },

            {

              "text": "nevermind"

            }

          ],

          "intent": "Cancel"

        },

        "id": "3c237ccee584e6009771b393e35cd599",

        "key": "3c237ccee584e6009771b393e35cd599",

        "value": {

          "rev": "1-e0707582e082e902ccf47dd68ab11afd"

        }

        },

        {

This means your successfully get all data from pets-database database 

12. Now we will create API Ends Point for this function

click Endpoints in the navigation menu.

Click the checkbox for Enable as Web Action.

Return to the main IBM Cloud Functions page

Click APIs

Click on pencil icon just front of api in right side which we created for post method this time we will use existing one.

Click on Define and Secure which is on the right side of the page

Click Create operation.

Put /get-data as the path.

Use GET for the verb, choose Package containing action API-Methods and choose Get-Pets-Data for the action.

Click Create.

Scroll to the bottom and click Save.

The API route. This is the domain and the base path.

And this is your end point

https://c0757ccd.eu-gb.apigw.appdomain.cloud/api/get-data

You have successfully created API End Point for getting data from pets-database database now you can check in postman or any an API client which is used to create, share, test and document APIs.

## 4. Create and Invoke an Action for Both GET AND POST and API End Points using Python 3.7

1.	Go to the IBM Cloud Functions UI.
2.	Click Start Creating.
3.	Click Action in order to create an action.
4.	Name your action Post-Pets-Data or whatever you want
5.	Click Create Package in order to put this action into a package.
6.	Name the package API-Methods since we'll make several actions that perform string manipulation.
7.	Click Create.
8.	Choose a Python 3.7 runtime 
9.	Click Create.
10.	Use the following code for the main function in the code editor.

          from cloudant.client import Cloudant

         def main(dict):

         screat={

         "COUCH_URL": "https://      apikey-v2-2fbmf63x6xo44u7xfpsejdvomi04eh4olv3699sf4f15:671cf3aaf1367d38a62e418a868df406@cc7d1ed4-5adc-4b06-b9b5-5405f4882ef0-bluemix.   cloudantnosqldb.appdomain.cloud",

         "IAM_API_KEY": " 3_8123R2ujttG90_i59V1joUYz-aalHagxXuo3Sem8Gn",

         "COUCH_USERNAME": "apikey-v2-2fbmf63x6xo44u7xfpsejdvomi04eh4olv3699sf4f15"

         }

         client = Cloudant.iam(
            account_name= screat["COUCH_USERNAME"],
            api_key= screat["IAM_API_KEY"],
            connect=True,
         )

         my_database = client["pets-database"]
         my_document = my_database.create_document(dict)
         if my_document.exists():
         return {"submitted":True}

11. Click Save.

12.	Click Invoke with parameters
13.	Put the following in the input box:

        {

          "intent": "Cancel",

         "examples": [

        {

          "text": "cancel that"

        },

        {

          "text": "cancel the request"

        },

        {
          "text": "forget it"

        },

        {

          "text": "i changed my mind"

        },

        {

          "text": "i don't want a table anymore anymore"

        },

        {

          "text": "never mind"

        },

        {

          "text": "nevermind"

        }

         ],

         "description": "Cancel the current request"

         }


14. Click Apply.
15.	Click Invoke.

You will see this output 

     {
       "submitted": True
     }

This means your data successfully inserted into the pets-database database you can see in your pets-database database one extra documents created

16. Now we will create API Ends Point for this function

click Endpoints in the navigation menu.

Click the checkbox for Enable as Web Action.

Return to the main IBM Cloud Functions page

Click APIs.

Click Create API.

For API name, put api. This will also create the base path.

Click Create operation.

Put /post-data as the path.

Use POST for the verb, choose Package containing action API-Methods and choose Post-Pets-Data for the action.

Click Create.

Scroll to the bottom and click Save.

The API route. This is the domain and the base path.

And this is your end point

https://c0757ccd.eu-gb.apigw.appdomain.cloud/api/post-data

You have successfully created API End Point for inserting data into pets-database database now you can check in postman or any an API client which is used to create, share, test and document APIs.

## Get method to get Data from Cloudant Database pets-database 

1.	Go to the IBM Cloud Functions UI.
2.	Click Start Creating.
3.	Click Action in order to create an action.
4.	Name your action Get-Pets-Data or whatever you want
5.	Select Package API-Methods in order to put this action into a package.
6.	This time do not have to create package again we selected existing package.
7.	Choose a Python 3.7 runtime 
8.	Click Create.

Use the following code for the main function in the code editor.

     from cloudant.client import Cloudant

     def main(dict):

     Retrive_data=[]

     screat={

     "COUCH_URL": "https://apikey-v2-2fbmf63x6xo44u7xfpsejdvomi04eh4olv3699sf4f15:671cf3aaf1367d38a62e418a868df406@cc7d1ed4-5adc-4b06-b9b5-5405f4882ef0-bluemix.cloudantnosqldb.appdomain.cloud",

     "IAM_API_KEY": " 3_8123R2ujttG90_i59V1joUYz-aalHagxXuo3Sem8Gn",

     "COUCH_USERNAME": "apikey-v2-2fbmf63x6xo44u7xfpsejdvomi04eh4olv3699sf4f15"

     }

     client = Cloudant.iam(

            account_name= screat["COUCH_USERNAME"],

            api_key= screat["IAM_API_KEY"],

            connect=True,

        )

        my_database = client["pets-database"]

     for document in my_database:

     Retrive_data.append(document)

     return({"Docs": Retrive_data})  

9.	Click Save.

10.	Click Invoke.

You will see this output 

         {

         "Docs":  [

         {

          "_id": "3c237ccee584e6009771b393e35cd599",

          "_rev": "1-e0707582e082e902ccf47dd68ab11afd",

          "description": "Cancel the current request",

          "examples": [

            {

              "text": "cancel that"

            },

            {

              "text": "cancel the request"

            },

            {

              "text": "forget it"

            },

            {

              "text": "i changed my mind"

            },

            {

              "text": "i don't want a table anymore anymore"

            },

            {

              "text": "never mind"

            },

            {

              "text": "nevermind"

            }

             ],
             "intent": "Cancel"
            },
             {
             "_id": "465da2d3e7ec3a90f35bad8f614cb6b4",
             "_rev": "1-ac322880dacb7926098ebc53c6d0c0f6",

	So on …

This means your successfully get all data from pets-database database 

11. Now we will create API Ends Point for this function

click Endpoints in the navigation menu.

Click the checkbox for Enable as Web Action.

Return to the main IBM Cloud Functions page

Click APIs

Click on pencil icon just front of api in right side which we created for post method this time we will use existing one.

Click on Define and Secure which is on the right side of the page

Click Create operation.

Put /get-data as the path.

Use GET for the verb, choose Package containing action API-Methods and choose Get-Pets-Data for the action.

Click Create.

Scroll to the bottom and click Save.

The API route. This is the domain and the base path.

And this is your end point

https://c0757ccd.eu-gb.apigw.appdomain.cloud/api/get-data

You have successfully created API End Point for getting data from pets-database database now you can check in postman or any an API client which is used to create, share, test and document APIs.













